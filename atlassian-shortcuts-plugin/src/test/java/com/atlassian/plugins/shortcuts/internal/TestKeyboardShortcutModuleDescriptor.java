package com.atlassian.plugins.shortcuts.internal;

import com.atlassian.plugin.AutowireCapablePlugin;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginInformation;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.PluginState;
import com.atlassian.plugin.Resourced;
import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.elements.ResourceLocation;
import static com.atlassian.plugins.shortcuts.api.KeyboardShortcutOperation.OperationType;

import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugins.shortcuts.api.KeyboardShortcut;
import com.atlassian.plugins.shortcuts.api.KeyboardShortcutModuleDescriptor;
import junit.framework.TestCase;
import org.dom4j.*;
import org.dom4j.tree.DefaultElement;

import java.io.InputStream;
import java.net.URL;
import java.util.*;

//TODO - Can we set the module descriptors from a test xml file like AbstractModuleDescriptorTest does in Confluence?
public class TestKeyboardShortcutModuleDescriptor extends TestCase
{
    private Document document;

    public void setUp() throws Exception
    {
        super.setUp();
        document = DocumentHelper.parseText(
                          "<keyboard-shortcut key=\"goto.dashboard\" i18n-name=\"admin.keyboard.shortcut.goto.dashboard.name\"\n"
                        + "                       name=\"Goto Dashboard\" state='enabled'>\n"
                        + "        <description key=\"admin.keyboard.shortcut.goto.dashboard.desc\">Go to Dashboard</description>\n"
                        + "        <shortcut>gd</shortcut>\n"
                        + "        <operation type=\"followLink\">#home_link</operation>\n"
                        + "    </keyboard-shortcut>");
    }

    public void testInit() throws DocumentException
    {
        final KeyboardShortcut keyboardShortcut = intiModuleDescriptor();
        
        assertEquals("global", keyboardShortcut.getContext());
        assertEquals(OperationType.followLink, keyboardShortcut.getOperation().getType());
        assertEquals("#home_link", keyboardShortcut.getParameter());
        assertEquals("[[g, d]]", keyboardShortcut.getShortcuts().toString());
    }

    public void testMultipleShortcuts() throws DocumentException
    {
        addElementToDocument("shortcut", "c");
        addElementToDocument("shortcut", "d");
        final KeyboardShortcut keyboardShortcut = intiModuleDescriptor();

        Set<List<String>> expected = new LinkedHashSet<List<String>>();
        List<String> shortcut1 = new ArrayList<String>();
        shortcut1.add("g");
        shortcut1.add("d");
        expected.add(shortcut1);
        List<String> shortcut2 = new ArrayList<String>();
        shortcut2.add("c");
        expected.add(shortcut2);
        List<String>shortcut3 = new ArrayList<String>();
        shortcut3.add("d");
        expected.add(shortcut3);

        assertEquals(3, expected.size());
        assertEquals(expected, keyboardShortcut.getShortcuts());
    }

    public void testInitWithContext() throws DocumentException
    {
        addElementToDocument("context", "issue");
        final KeyboardShortcut keyboardShortcut = intiModuleDescriptor();

        assertEquals("issue", keyboardShortcut.getContext());
    }

    public void testOrder() throws DocumentException
    {
        addElementToDocument("order", "20");
        final KeyboardShortcut keyboardShortcut = intiModuleDescriptor();

        assertEquals(20, keyboardShortcut.getOrder());
    }

    public void testOperationType()
    {
        changeDocumentAttribute("//keyboard-shortcut/operation", "type", "click");
        final KeyboardShortcut keyboardShortcut = intiModuleDescriptor();

        assertEquals(OperationType.click, keyboardShortcut.getOperation().getType());
    }

    public void testMissingShortcut() throws DocumentException
    {
        removeElementFromDocument("//keyboard-shortcut/shortcut");
        try
        {
            final KeyboardShortcut keyboardShortcut = intiModuleDescriptor();
            fail("Should have thrown exception.");
        }
        catch (PluginParseException e)
        {
            assertEquals(e.getMessage(), "<shortcut> is a required element for a keyboard shortcut plugin module");
        }


    }

    public void testEmptyShortcut() throws DocumentException
    {
        changeElementText("//keyboard-shortcut/shortcut", "");
        try
        {
            final KeyboardShortcut keyboardShortcut = intiModuleDescriptor();
            fail("Should have thrown exception.");
        }
        catch (PluginParseException e)
        {
            assertEquals(e.getMessage(), "The <shortcut> element did not provide a keyboard shortcut definition");
        }
    }

    public void testMissingOperation()
    {
        removeElementFromDocument("//keyboard-shortcut/operation");
        try
        {
            final KeyboardShortcut keyboardShortcut = intiModuleDescriptor();
            fail("Should have thrown exception.");
        }
        catch (PluginParseException e)
        {
            assertEquals(e.getMessage(), "<operation> is a required element for a keyboard shortcut plugin module");
        }
    }

    public void testInvalidOperation() throws DocumentException
    {
        changeDocumentAttribute("//keyboard-shortcut/operation", "type", "booyah");
        try
        {
            final KeyboardShortcut keyboardShortcut = intiModuleDescriptor();
            fail("Should have thrown exception.");
        }
        catch (PluginParseException e)
        {
            assertEquals(e.getMessage(), "Invalid operation type 'booyah' provided");
        }
    }

    public void testMissingI18nDescriptionKey()
    {
        removeDocumentAttribute("//keyboard-shortcut/description", "key");
        try
        {
            final KeyboardShortcut keyboardShortcut = intiModuleDescriptor();
            fail("Should have thrown exception.");
        }
        catch (PluginParseException e)
        {
            assertEquals(e.getMessage(), "<description> i18n 'key' attribute is a required attribute for a keyboard shortcut plugin module");
        }        
    }

    public void tearDown()
    {
        document = null;
    }

    private KeyboardShortcut intiModuleDescriptor()
    {
        KeyboardShortcutModuleDescriptor moduleDescriptor = new KeyboardShortcutModuleDescriptor(ModuleFactory.LEGACY_MODULE_FACTORY);
        moduleDescriptor.init(new MockPlugin("keyboard-shortcuts"), document.getRootElement());
        return moduleDescriptor.getModule();
    }
    
    private void removeDocumentAttribute(String xPath, String attributeName)
    {
        List<Node> nodes = document.selectNodes(xPath);
        Element element = (Element)nodes.get(0);
        Attribute attribute = element.attribute(attributeName);
        element.remove(attribute);
    }

    private void addElementToDocument(String name, String text)
    {
        Element secondShortcutElement = new DefaultElement(name);
        secondShortcutElement.addText(text);
        document.getRootElement().add(secondShortcutElement);
    }
    
    private void removeElementFromDocument(String xPath)
    {
        List<Node> nodes = document.selectNodes(xPath);
        document.getRootElement().remove(nodes.get(0));
    }

    private void changeElementText(String xPath, String newText)
    {
        List<Node> nodes = document.selectNodes(xPath);
        nodes.get(0).setText(newText);
    }

    private void changeDocumentAttribute(String xPath, String attribute, String value)
    {
        List<Node> nodes = document.selectNodes(xPath);
        Element element = (Element)nodes.get(0);
        element.attribute(attribute).setText(value);
    }

    public static class MockPlugin implements Plugin, AutowireCapablePlugin
    {
        String key;

        public MockPlugin(final String key)
        {
            this.key = key;
        }

        @Override
        public boolean equals(final Object o)
        {
            if (this == o)
            {
                return true;
            }
            if ((o == null) || (getClass() != o.getClass()))
            {
                return false;
            }

            final MockPlugin that = (MockPlugin) o;

            if (key != null ? !key.equals(that.key) : that.key != null)
            {
                return false;
            }

            return true;
        }

        public int getPluginsVersion()
        {
            return 0;
        }

        public void setPluginsVersion(final int version)
        {}

        public String getName()
        {
            return null;
        }

        public void setName(final String name)
        {}

        public String getI18nNameKey()
        {
            return null;
        }

        public void setI18nNameKey(final String i18nNameKey)
        {}

        public String getKey()
        {
            return key;
        }

        public void setKey(final String aPackage)
        {}

        public void addModuleDescriptor(final ModuleDescriptor<?> moduleDescriptor)
        {}

        public Collection<ModuleDescriptor<?>> getModuleDescriptors()
        {
            return null;
        }

        public ModuleDescriptor<?> getModuleDescriptor(final String key)
        {
            return null;
        }

        public <M> List<ModuleDescriptor<M>> getModuleDescriptorsByModuleClass(final Class<M> moduleClass)
        {
            return null;
        }

        public boolean isEnabledByDefault()
        {
            return false;
        }

        public void setEnabledByDefault(final boolean enabledByDefault)
        {}

        public PluginInformation getPluginInformation()
        {
            return null;
        }

        public void setPluginInformation(final PluginInformation pluginInformation)
        {}

        public void setResources(final Resourced resources)
        {}

        public PluginState getPluginState()
        {
            return null;
        }

        public boolean isEnabled()
        {
            return false;
        }

        public void setEnabled(final boolean enabled)
        {}

        public boolean isSystemPlugin()
        {
            return false;
        }

        public boolean containsSystemModule()
        {
            return false;
        }

        public void setSystemPlugin(final boolean system)
        {}

        public boolean isBundledPlugin()
        {
            return false;
        }

        public Date getDateLoaded()
        {
            return null;
        }

        public boolean isUninstallable()
        {
            return false;
        }

        public boolean isDeleteable()
        {
            return false;
        }

        public boolean isDynamicallyLoaded()
        {
            return false;
        }

        public <T> Class<T> loadClass(final String clazz, final Class<?> callingClass) throws ClassNotFoundException
        {
            return null;
        }

        public ClassLoader getClassLoader()
        {
            return null;
        }

        public URL getResource(final String path)
        {
            return null;
        }

        public InputStream getResourceAsStream(final String name)
        {
            return null;
        }

        public void close()
        {}

        public <T> T autowire(final Class<T> clazz)
        {
            return null;
        }

        public <T> T autowire(final Class<T> clazz, final AutowireStrategy autowireStrategy)
        {
            return null;
        }

        public void autowire(final Object instance)
        {}

        public void autowire(final Object instance, final AutowireStrategy autowireStrategy)
        {}

        public List<ResourceDescriptor> getResourceDescriptors()
        {
            return null;
        }

        public List<ResourceDescriptor> getResourceDescriptors(final String type)
        {
            return null;
        }

        public ResourceDescriptor getResourceDescriptor(final String type, final String name)
        {
            return null;
        }

        public ResourceLocation getResourceLocation(final String type, final String name)
        {
            return null;
        }

        public int compareTo(final Plugin o)
        {
            return 0;
        }

        public Set<String> getRequiredPlugins()
        {
            return Collections.emptySet();
        }

        public void disable()
        {}

        public void enable()
        {}

        public void install()
        {}

        public void uninstall()
        {}
    }
}
