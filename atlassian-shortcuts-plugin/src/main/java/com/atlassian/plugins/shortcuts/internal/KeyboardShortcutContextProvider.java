package com.atlassian.plugins.shortcuts.internal;

import java.security.Principal;

/**
 * Defines if the user has access to use keyboard shortcuts in the given host application.
 * @since v1.0
 */
//TODO - implement this
public interface KeyboardShortcutContextProvider
{
    /**
     * Determines if a particular user has access to use keyboard shortcuts.  Generally this will just return true,
     * however host applications may choose to give users the ability to disable keyboard shortcuts.
     *
     * @param user the user viewing a page in the hostapplication
     * @return true if the user should be allowed to use keyboard shortcuts.
     */
    boolean hasPermission(Principal user);
}
